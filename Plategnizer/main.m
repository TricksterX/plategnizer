//
//  main.m
//  Plategnizer
//
//  Created by Guilherme Campos on 7/11/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MLAppDelegate class]));
    }
}
