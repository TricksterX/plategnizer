//
//  MLAutoViewController.h
//  Plategnizer
//
//  Created by Guilherme Campos on 7/12/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>
#import "MLMainViewController.h"

@interface MLAutoViewController : MLMainViewController <TesseractDelegate>
@property (strong, nonatomic) IBOutlet UIView *cameraPlate;
@property (strong, nonatomic) IBOutlet UILabel *resultPlate;

@end
