//
//  MLMainViewController.m
//  Plategnizer
//
//  Created by Guilherme Campos on 7/11/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import "MLMainViewController.h"

@interface MLMainViewController ()

@end

@implementation MLMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (NSString *) validatePlateFormat: (NSString *) recognizedPlate{
    NSString *plate = recognizedPlate ;
    NSMutableArray *plateArray = [NSMutableArray array];

    int separatorsFound = 0;

    for (int i = 0; i < [plate length]; i ++){
        [plateArray addObject:[NSString stringWithFormat:@"%C",[plate characterAtIndex:i]]];
    }

    for (int i = 0; i < [plateArray count]; i ++){
        //must be a letter
        if (i < 3){
            if ([[plateArray objectAtIndex:i] isEqualToString:@"0"]){
                [plateArray replaceObjectAtIndex:i withObject:@"O"];
            } else if ([[plateArray objectAtIndex:i] isEqualToString:@"8"]){
                [plateArray replaceObjectAtIndex:i withObject:@"B"];
            } else if ([[plateArray objectAtIndex:i] isEqualToString:@"-"]){
                [plateArray removeObjectAtIndex:i];
            }
        }

        if ([[plateArray objectAtIndex:i] isEqualToString:@"-"]){
            separatorsFound ++;

            if (separatorsFound > 1){
                [plateArray removeObjectAtIndex:i];
            }
        }

        //must be a number
        if (i >= 3 && [plateArray count] > i){
            if ([[plateArray objectAtIndex:i] isEqualToString:@"B"]){
                [plateArray replaceObjectAtIndex:i withObject:@"8"];
            } else if ([[plateArray objectAtIndex:i] isEqualToString:@"O"]){
                [plateArray replaceObjectAtIndex:i withObject:@"0"];
            }
        }
    }

    return [[plateArray valueForKey:@"description"] componentsJoinedByString:@""];
}

@end
