//
//  MLManualViewController.m
//  Plategnizer
//
//  Created by Guilherme Campos on 7/12/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import "MLManualViewController.h"
#import "ImageProcessingImplementation.h"

@interface MLManualViewController ()

@end

@implementation MLManualViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Image Capturing
- (IBAction)captureImage:(id)sender {
    _imagePicker = [UIImagePickerController new];
    [_imagePicker setDelegate:self];
    [_imagePicker setAllowsEditing:YES];

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Escolha uma foto"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancelar"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"Tirar Foto", @"Escolher Existente", nil];
        [actionSheet showInView:self.view];
    } else {
        [_imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != actionSheet.cancelButtonIndex){
        if (buttonIndex == 0)
            [_imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        else if (buttonIndex == 1)
            [_imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:_imagePicker animated:YES completion:nil];
    } else
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *original = [info objectForKey:UIImagePickerControllerOriginalImage];
    _imagePlate = original;

    [self plategnize];
}



#pragma mark - Tesseract

- (void) plategnize
{
    Tesseract* tesseract = [[Tesseract alloc] initWithLanguage:@"eng_plate"];
    tesseract.delegate = self;

    [tesseract setVariableValue:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-" forKey:@"tessedit_char_whitelist"]; //limit search
    [tesseract setVariableValue:@"1" forKey:@"language_model_penalty_non_freq_dict_word" ];
    [tesseract setVariableValue:@"1" forKey:@"language_model_penalty_non_dict_word" ];
    [tesseract setVariableValue:@"true" forKey:@"segment_penalty_dict_" ];
    [tesseract setVariableValue:@"true" forKey:@"PSM_SINGLE_COLUMN" ];

    [_originalPlate setImage:_imagePlate];

    _imagePlate = [[ImageProcessingImplementation new] processImage:_imagePlate];
    [_finalPlate setImage:_imagePlate];

    [tesseract setImage:_imagePlate];
    [tesseract recognize];

    [_resultPlate setText: [NSString stringWithFormat:@"Resultado: %@", [self validatePlateFormat: [tesseract recognizedText]]] ];

    tesseract = nil; //deallocate and free all memory
}

- (BOOL)shouldCancelImageRecognitionForTesseract:(Tesseract*)tesseract
{
    NSLog(@"progress: %d", tesseract.progress);
    return NO;  // return YES, if you need to interrupt tesseract before it finishes
}

@end
