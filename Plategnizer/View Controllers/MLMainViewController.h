//
//  MLMainViewController.h
//  Plategnizer
//
//  Created by Guilherme Campos on 7/11/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLMainViewController : UIViewController

- (NSString *) validatePlateFormat: (NSString *) recognizedPlate;

@end
