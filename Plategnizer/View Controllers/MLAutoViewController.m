//
//  MLAutoViewController.m
//  Plategnizer
//
//  Created by Guilherme Campos on 7/12/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import "MLAutoViewController.h"

@interface MLAutoViewController ()

@end

@implementation MLAutoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (UIImage*)processImage:(UIImage*)src
{
    return src;
}

- (BOOL)shouldCancelImageRecognitionForTesseract:(Tesseract*)tesseract
{
    NSLog(@"progress: %d", tesseract.progress);
    return NO;  // return YES, if you need to interrupt tesseract before it finishes
}

@end
