//
//  MLManualViewController.h
//  Plategnizer
//
//  Created by Guilherme Campos on 7/12/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>
#import "MLMainViewController.h"

@interface MLManualViewController : MLMainViewController <TesseractDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

- (IBAction)captureImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *originalPlate;
@property (strong, nonatomic) IBOutlet UIImageView *finalPlate;
@property (strong, nonatomic) IBOutlet UILabel *resultPlate;

@property (strong, nonatomic) UIImagePickerController *imagePicker;

@property (strong, nonatomic) UIImage * imagePlate;

@end
