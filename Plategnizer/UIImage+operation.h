//
//  UIImage+operation.h
//  Plategnizer
//
//  Created by Guilherme Campos on 7/12/14.
//  Copyright (c) 2014 Mowa Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UKImage)

- (UIImage*)rotate:(UIImageOrientation)orientation;

@end

