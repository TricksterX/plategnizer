//
//  ImageProcessingImplementation.h
//  ANPR
//
//  Created by Christian Roman on 29/08/13.
//  Copyright (c) 2013 Christian Roman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageProcessingImplementation : NSObject 

- (UIImage*)processImage:(UIImage*)src;

@end
